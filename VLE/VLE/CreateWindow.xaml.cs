﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using VLE.DataAccess.Repositories;
using VLE.Models;

namespace VLE
{
    /// <summary>
    /// Interaction logic for CreateWindow.xaml
    /// </summary>
    public partial class CreateWindow : Window
    {
        public CreateWindow(Client client)
        {
            Client = client;
            InitializeComponent();
        }

        private Client Client { get; set; }
        private void Create_Click(object sender, RoutedEventArgs e)
        {
            if (CourseName.Text.Length == 0 || Text.Text.Length == 0)
            {
                MessageBox.Show("Fill all fields!");
            }
            else
            {

                using (VleContext repository = new VleContext())
                {
                    Course course = new Course(CourseName.Text, string.Format("{0} {1}",Client.FirstName,Client.LastName),Text.Text);
                    repository.Courses.Add(course);
                    repository.SaveChanges();
                    MainWindow main = new MainWindow(Client);
                    main.Show();
                    Close();
                }
                Close();
            }
        }
    }
}
