﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using VLE.DataAccess.Repositories;
using VLE.Models;

namespace VLE
{
    /// <summary>
    /// Interaction logic for RemoveWindow.xaml
    /// </summary>
    public partial class RemoveWindow : Window
    {
        public RemoveWindow(Client client)
        {
            Client = client;
            InitializeComponent();
        }
        private Client Client { get; set; }

        private void Remove_Click(object sender, RoutedEventArgs e)
        {
            if (CourseName.Text.Length == 0)
            {
                MessageBox.Show("Fill all fields!");
            }
            else
            {

                using (VleContext repository = new VleContext())
                {
                    Course course = repository.Courses.First(p => p.Name == CourseName.Text);
                    repository.Courses.Remove(course);
                    repository.SaveChanges();
                    MainWindow main = new MainWindow(Client);
                    main.Show();
                    Close();
                }
                Close();
            }
        }
    }
}
