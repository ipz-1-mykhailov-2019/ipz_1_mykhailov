﻿using System.Linq;
using System.Windows;
using VLE.DataAccess.Repositories;
using VLE.Models;

namespace VLE
{
    /// <summary>
    /// Interaction logic for LogIn.xaml
    /// </summary>
    public partial class LogIn : Window
    {
        public LogIn()
        {
            InitializeComponent();
        }

        private void LogIn_Click(object sender, RoutedEventArgs e)
        {
            if (Login.Text.Length == 0 || Password.Password.Length == 0)
            {
                MessageBox.Show("Fill all fields!");
            }
            else
            {
                using (VleContext repository = new VleContext())
                {
                    var clients = repository.Clients.ToList();
                    int counter = clients.Count;
                    Client c = new Client();
                    foreach (var client in clients)
                    {
                        if (client.Login != Login.Text || client.Password != Password.Password.ToString())
                        {
                            counter--;
                        }
                        else
                        {
                            c = client;
                        }
                    }
                    if (counter == 0)
                    {
                        MessageBox.Show("Incorrect login or password!");
                    }
                    else
                    {
                        if (c.Role == "Teacher")
                        {
                            MainWindow mainWindow = new MainWindow(c);
                            mainWindow.Show();
                            Close();
                        }
                        else if (c.Role == "Student")
                        {
                            StudentWindow studentWindow = new StudentWindow(c);
                            studentWindow.Show();
                            Close();
                        }
                    }
                }
            }
        }

        private void Reg_Click(object sender, RoutedEventArgs e)
        {
            RegisterWindow reg = new RegisterWindow();
            reg.Show();
            Close();
        }
    }
}
