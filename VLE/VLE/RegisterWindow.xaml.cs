﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using VLE.DataAccess.Repositories;
using VLE.Models;

namespace VLE
{
    /// <summary>
    /// Interaction logic for RegisterWindow.xaml
    /// </summary>
    public partial class RegisterWindow : Window
    {
        public RegisterWindow()
        {
            InitializeComponent();
        }

        private void Reg_Click(object sender, RoutedEventArgs e)
        {
            if (FirstName.Text.Length == 0 || LastName.Text.Length == 0 || Role.Text.Length == 0 || Login.Text.Length == 0 || Password.Password.Length == 0)
            {
                MessageBox.Show("Fill all fields!");

            }
            else
            {

                using (VleContext repository = new VleContext())
                {
                    Client client = new Client(FirstName.Text, LastName.Text, Role.Text, Login.Text, Password.Password.ToString());
                    repository.Clients.Add(client);
                    repository.SaveChanges();
                    LogIn main = new LogIn();
                    main.Show();
                    Close();
                }
                Close();
            }
        }
        private void Back_Click(object sender, RoutedEventArgs e)
        {
            LogIn logIn = new LogIn();
            logIn.Show();
            Close();
        }
    }
}
