﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using VLE.DataAccess.Repositories;
using VLE.Models;

namespace VLE
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {


        public MainWindow()
        {
            InitializeComponent();
        }

        public MainWindow(Client client)
        {
            Client = client;
            InitializeComponent();
        }

        public Client Client { get; set; }

        private void Show_Click(object sender, RoutedEventArgs e)
        {
            StudentWindow2 courseList = new StudentWindow2(Client);
            courseList.Show();
            Close();
        }

        private void Create_Click(object sender, RoutedEventArgs e)
        {
            CreateWindow createWindow = new CreateWindow(Client);
            createWindow.Show();
            Close();
        }

        private void Remove_Click(object sender, RoutedEventArgs e)
        {
            RemoveWindow removeWindow = new RemoveWindow(Client);
            removeWindow.Show();
            Close();
        }
        private void Back_Click(object sender, RoutedEventArgs e)
        {
            LogIn logIn = new LogIn();
            logIn.Show();
            Close();
        }
    }
}
