﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VLE.Models
{
    public class Course
    {
        public Course() { }
        public Course(string name, string author, string text)
        {
            Name = name;
            Author = author;
            Text = text;
        }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }

        public string Name { get; set; }

        public string Author { get; set; }

        public string Text { get; set; }
    }
}
