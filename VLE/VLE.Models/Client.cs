﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;


namespace VLE.Models
{
    public class Client
    {
        public Client(string firstName, string lastName, string role, string login, string password)
        {
            FirstName = firstName;
            LastName = lastName;
            Role = role;
            Login = login;
            Password = password;
        }

        public Client()
        {

        }
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string Login { get; set; }

        public string Password { get; set; }

        public string Role { get; set; }

    }


}
